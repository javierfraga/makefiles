#include <stdio.h>
/*#include "gd.c"*/
#include "gd.h"

/*double get_double( char *promt , double min , double max );*/

int main(int argc, char *argv[])
{
    double price , tip;
    price = get_double((char *)("Enter price meal:"), 0 , 1000);
    /*printf("Enter price meal:");*/
    /*scanf("%lf", &price);*/

    tip = get_double((char *)("Enter tip amount (percent): "), 0 , 100);
    /*printf("Enter tip amount (percent): ");*/
    /*scanf("%lf", &tip);*/

    double timpAmt = price * tip / 100.0;
    double total = price + timpAmt;
    printf("Tip amount: %lf\n", timpAmt);
    printf("Total amount: %lf\n", total);

    return 0;
}
