#include <stdio.h>
#include <math.h>
/*#include "gd.c"*/
#include "gd.h"

/*double get_double( char *promt , double min , double max );*/

int main(int argc, char *argv[])
{
    double x = get_double( (char *)("Enter the x value: ") , -100 , 100 );
    double y = get_double( (char *)("Enter the y value: ") , -100 , 100 );

    double d = sqrt( pow(x,2) + pow(y,2) );
    printf("Distance is %lf meters\n", d);
    return 0;
}
